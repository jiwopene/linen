# Pipe line count

Simple C program that shows number of lines processed in bash pipe

## Examples

Show file count in home direcotry:

    find ~ | linen > /dev/null

Show count of new lines in Apache log file:

    tail -fn0 /var/log/apache2/access_log | linen > /dev/null


## Installation

You do not need any special libraries to build on Linux and probably other operating systems.


    # If you are root, use this:
    make install

    # On most Linux systems (like Ubuntu) you need to use sudo:
    make && sudo make install
