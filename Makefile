CFLAGS:=-O3

INSTALL := install

all: linen

linen: main.c
	$(CC) -o $@ $< $(CFLAGS)

clean:
	$(RM) -v linen

install: linen
	$(INSTALL) -s -o root -g root $< -t /usr/local/bin

uninstall:
	$(RM) -v /usr/local/bin/linen
