#include <stdio.h>

int main(int argc, void* argv) {
	if (argc > 1) {
		puts(
			"Line counter. Do not pass any arguments.\n"
			"Every input line is repeated on stdout (like the cat command). Line count is printed on stderr.\n"
		);
	}
	else {
		int lines = 0;
		char input;

		fprintf(stderr, "Lines: 0");

		while (fread(&input, 1, 1, stdin) > 0) {
			putchar(input);
			if (input == '\n') {
				fputc('\b', stderr);
				for (int lines_tmp = lines; lines_tmp > 9; lines_tmp /= 10)
					fputc('\b', stderr);
				lines++;
				fprintf(stderr, "%d", lines);
			}
		}
	}
}


